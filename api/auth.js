const express = require('express');
const router = express.Router();
const Auth = require('../models/Auth');
const { errorResponse, successResponse } = require('../helpers/response')

/* 
* @method: GET 
* @description: Auth post request values
* @status: Done
*/
router.post('/', async (req, res) => {
  try {
    const newAuth = new Auth({
      username: req.body.username,
      password: req.body.password,
      created_at: new Date()
    });

    const authResponse = await newAuth.save();
    // res.json(authResponse)
    return successResponse(res, authResponse, 'Login successfully.')
  } catch (error) {
    if (!req.body.username) {
      const message = 'Username is required.'
      return errorResponse(res, error, message);
    } else if (!req.body.password) {
      const message = 'Password is required.'
      return errorResponse(res, error, message);
    }
  }
})

module.exports = router;