const errorResponse = (res, error, message, statusCode = 500) => {
  return res.status(statusCode).json({
    code: statusCode,
    message: message
  })
}

const successResponse = (res, data, message, statusCode = 200) => {
  return res.status(statusCode).json({
    code: statusCode,
    message: message
  })
}

const adlritsFunction = (res) => {
  console.log(res)
}

module.exports = { errorResponse, successResponse }