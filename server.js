const express = require('express');
const app = express();
const port = 3000;
const bodyParser = require('body-parser');
const connectDB = require('./config/db.js');
const cors = require('cors')
require('dotenv/config');

app.use(bodyParser.json());

// Connect to DB
connectDB();

// Import routes
app.use('/auth', require('./api/auth.js'));


app.listen(port, () => console.log(`server started at ${port}`));